include eloquentxx.inc

inherit cmake pkgconfig

DEPENDS = "sqlite3"

SRC_URI = "git://git@gitlab.com/aries-project/software/libraries/eloquentxx.git;protocol=ssh;branch=develop"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "35344e4f75172379479c520b5eca3e7055f16abc"

S = "${WORKDIR}/git"

#inherit externalsrc
#EXTERNALSRC = "/mnt/cedev/workspace/aries/eloquentxx"