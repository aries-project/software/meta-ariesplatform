DESCRIPTION = " "
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "wampcc curl"

inherit cmake pkgconfig

SRC_URI = "git://gitlab.com/aries-project/software/libraries/libnexus.git;protocol=https;branch=develop"
SRC_URI[sha256sum] = "de89b8a156058ea5c56ae3534d7eea09e13ccd6b66e3abb6818d3070cf04546c"
SRCREV = "1be1a6148bd14a7e92c0058ce82019c59b9b309d"

S = "${WORKDIR}/git"
