DESCRIPTION = "Jansson is a C library for encoding, decoding and manipulating JSON data."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit autotools pkgconfig

SRC_URI = "http://digip.org/jansson/releases/jansson-${PV}.tar.bz2"

