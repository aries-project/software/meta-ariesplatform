DESCRIPTION = "RaspberryPi C++ IO library"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "bcm2835"

inherit cmake pkgconfig

SRC_URI = "git://gitlab.com/colibri-embedded/pi4xx.git;protocol=https;branch=develop"
SRCREV = "a13673f3e7a2874745fdb8277e2b9491fbe4748b"

S = "${WORKDIR}/git"
