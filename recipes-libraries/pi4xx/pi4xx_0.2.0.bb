DESCRIPTION = "RaspberryPi C++ IO library"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "bcm2835"

inherit cmake pkgconfig

SRC_URI = "https://gitlab.com/colibri-embedded/pi4xx/-/archive/v${PV}/pi4xx-v${PV}.tar.bz2"
SRC_URI[sha256sum] = "235f4d8821e5671664097f3538877eaed8a4c2c18df3813f8bbe273f3f25f6e4"

S = "${WORKDIR}/pi4xx-v${PV}"
