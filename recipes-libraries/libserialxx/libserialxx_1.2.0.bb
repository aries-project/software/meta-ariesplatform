DESCRIPTION = "Serial port library written in C++ with support for higher layers of communication."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit cmake pkgconfig

SRC_URI = "https://gitlab.com/colibri-embedded/libserialxx/-/archive/v${PV}/libserialxx-v${PV}.tar.bz2"
SRC_URI[sha256sum] = "2caf5fa883b3141a0d0277f7147ac41c23a367dc563499f3b3ce373957fb59df"

S = "${WORKDIR}/libserialxx-v${PV}"
