DESCRIPTION = "Serial port library written in C++ with support for higher layers of communication."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit cmake pkgconfig

SRC_URI = "git://gitlab.com/colibri-embedded/libserialxx.git;protocol=https;branch=develop"
SRCREV = "cc66a5071c9402bd65e102db5c2ed9890a504da8"

S = "${WORKDIR}/git"

