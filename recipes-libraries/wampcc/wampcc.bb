DESCRIPTION = "WAMP C++ library that provides the WAMP basic profile for client \
roles and also a lightweight implementation of a WAMP router / dealer."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit cmake pkgconfig

DEPENDS = "jansson openssl libuv"

SRC_URI = "git://github.com/darrenjs/wampcc.git;protocol=https;branch=master \
    file://0001-disable-ipv6-socket.patch \
    "

SRCREV = "1ab1b8b645f2869b1b88ea583ee80891b0070dd4"

S = "${WORKDIR}/git"


