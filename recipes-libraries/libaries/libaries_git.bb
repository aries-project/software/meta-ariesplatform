DESCRIPTION = "Aries low-level communication library"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "libserialxx"

inherit cmake pkgconfig

#inherit externalsrc
#EXTERNALSRC = "/mnt/cedev/workspace/aries/libaries/"
#EXTERNALSRC_BUILD = "${EXTERNALSRC}"

SRC_URI = "git://gitlab.com/aries-project/software/libraries/libaries.git;protocol=https;branch=develop"
SRCREV = "fb1884777389722a1907c1262d97b63a977f4cc6"

S = "${WORKDIR}/git"
