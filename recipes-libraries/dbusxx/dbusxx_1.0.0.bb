DESCRIPTION = "DBus C++ wrapper library"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "dbus"

inherit cmake pkgconfig

SRC_URI = "https://gitlab.com/colibri-embedded/dbusxx/-/archive/v${PV}/dbusxx-v${PV}.tar.bz2"
SRC_URI[sha256sum] = "ea157015a852f91cd09cdf9acd3aa0c50599d5dd13767dc6dc2dd80dcbb00c36"

S = "${WORKDIR}/dbusxx-v${PV}"