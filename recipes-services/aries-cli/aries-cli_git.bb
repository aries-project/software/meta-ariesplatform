# include aries-cli.inc
DESCRIPTION = "Aries CLI" 
LICENSE = "MIT" 
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "libserialxx libaries yaml-cpp pi4xx ncurses readline wampcc"

inherit cmake pkgconfig

SRC_URI = "git://gitlab.com/aries-project/software/services/aries-cli.git;protocol=https;branch=develop"
SRCREV = "7a0011f5017782507aaebc810efbd73a46f15f9b"

S = "${WORKDIR}/git"