include nexus-wamp-router.inc

inherit cmake pkgconfig

inherit externalsrc
EXTERNALSRC = "/mnt/cedev/workspace/aries/nexus-wamp-router"
EXTERNALSRC_BUILD = "${EXTERNALSRC}"

DEPENDS = "libnexus"