include aries-http-server.inc

inherit cmake pkgconfig

inherit externalsrc
EXTERNALSRC = "/mnt/cedev/workspace/aries/aries-http-server"
EXTERNALSRC_BUILD = "${EXTERNALSRC}"

DEPENDS = "wampcc eloquentxx"