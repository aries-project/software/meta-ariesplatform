SUMMARY = "Aries services"
DESCRIPTION = ""
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = "\
    nexus-wamp-router \
    aries-datalink-service \
    aries-http-server \
    "