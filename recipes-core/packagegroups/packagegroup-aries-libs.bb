SUMMARY = "Aries libraries"
DESCRIPTION = ""
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = "\
    libserialxx \
    libaries \
    dbusxx \
    pi4xx \
    libnexus \
    eloquentxx \
    yaml-cpp \
    bcm2835 \
    "