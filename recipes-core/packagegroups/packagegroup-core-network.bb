SUMMARY = "Network related tools"
DESCRIPTION = "The minimal set of packages required to boot the system"
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = "connman"