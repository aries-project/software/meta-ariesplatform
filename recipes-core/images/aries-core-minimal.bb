# Base this image on core-image-base
include recipes-core/images/core-image-base.bb

COMPATIBLE_MACHINE = "^rpi$"

TOOLCHAIN_HOST_TASK:append = " nativesdk-cmake"

IMAGE_FEATURES:append = " \
    ssh-server-openssh \
    package-management \
    "

# Extra packages

IMAGE_INSTALL:append = "\
    packagegroup-core-network \
    "

IMAGE_INSTALL:append = "\
    libserialxx \
    libaries \
    dbusxx \
    pi4xx \
    libnexus \
    eloquentxx \
    yaml-cpp \
    bcm2835 \
    "

IMAGE_INSTALL:append = "\
    nexus-wamp-router \
    aries-datalink-service \
    aries-http-server \
    "